# Statuts de l'Association

## Build

The non-profit organization *Statuts* document is written using LateX. To generate a PDF file, follow the instructions:

1. Install a LateX distribution. On Ubuntu, type:

    ```bash
    sudo apt install texlive texlive-formats-extra texlive-lang-french texlive-extra-utils
    ```

2. Build the PDF file:

    ```bash
    pdflatex status.tex -output-directory="./build"
    ```
