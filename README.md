![Solarus Labs logo](logo-solarus-labs.png)

# Solarus Labs

**Solarus Labs** is the French non-profit organization that promotes the free
and open-source game engine Solarus, as an *Association loi de 1901*.

Its goal is to develop, promote and train for the use of free software for video
games development.

This repository contains all the public documents necessary to its operation.

## Generate PDF files

The `.pdf` files are generated from `.md` files with `pandoc` and LateX.

1. Install `pandoc`:

    - Ubuntu:

      ```bash
      sudo apt install pdflatex
      sudo apt install pandoc
      ```

    - macOS:

      ```bash
      brew install coreutils
      brew install --cask mactex
      eval "$(/usr/libexec/path_helper)"
      brew install pandoc
      ```

    - Windows:

      Install MikteX:

      ```cmd
      choco install miktex
      ```

      Restart the console, then update MikteX and install the French package:

      ```cmd
      mpm --admin --update-db
      mpm --admin --upgrade --package-level=basic
      mpm --admin --update
      mpm --admin --install=babel-french
      ```

      Finally, installer Pandoc:

      ```powershell
      choco install pandoc
      refreshenv
      ```

2. Generate the PDF file:

    ```bash
    pandoc "input.md" -o "output.pdf"
    ```
