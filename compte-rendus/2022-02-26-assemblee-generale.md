# Procès-verbal de l'Assemblée Générale du 26/02/2022

## Introduction

Le 26/02/2022, les membres de l'Association *Solarus Labs* se sont réunis en Assemblée Générale ensemble et par visioconférence pour ceux qui ne pouvaient pas venir.

Etaient présents :

- Christophe Thiéry (sur place).
- Kévin Baumann.
- Olivier Cléro (sur place).
- Benjamin Schweitzer (sur place).
- Max Mraz (membre honorifique).
- Hugo Hromic (membre honorifique).

L'Assemblée Générale a désigné :

- Christophe Thiéry en qualité de Président de Séance.
- Olivier Cléro en qualité de Secrétaire de Séance.

Le Président de Séance a rappellé que l'Assemblée Générale était appelée à statuer sur l'ordre du jour suivant :

- [Rapport moral](#rapport-moral)
- [Rapport financier](#rapport-financier)
- [Élection des membres du Conseil d'Administration](#élection-des-membres-du-conseil-dadministration)
- [Fonctionnement de l'association](#fonctionnement-de-lassociation)

## Rapport moral

### Solarus Labs en 2021

#### Projets accomplis avec Solarus en 2021

- Solarus 1.6.5 :
  - Corrections de bugs.

- Évènements de la communauté :
  - Participation au Zeldathon.
  - Nouveau thème pour le forum.

- Nombreuses contributions venant de la communauté :
  - Packs de ressources.
  - Jeux.
  - Améliorations du moteur.

- Solarus a fait parler de lui :
  - Sortie d'*Ocean's Heart* : premier jeu commercial fait avec Solarus.
  - Portage Switch de Solarus, afin d'y faire tourner *Ocean's Heart*.

#### Projets ayant progressé en 2021

- Version 1.7 du moteur avec multijoueurs.
- Tutoriels vidéo.
- Nouveau Lanceur de quêtes Solarus.
- Thème sombre pour l'Éditeur de quêtes Solarus.
- *A Link to the Dream*.

### Projets envisagés en 2022

- Solarus ~~1.7~~ **2.0** :
  - Fonctionnalités multi-joueurs.
  - Nouveau lanceur Solarus.
  - Portage Android.
  - Portage WASM.
  - Style sombre pour l'Éditeur de quête Solarus.
  - Nouveau logo.
  - Rafraîchissement du site web.
  - Nouveau site web de documentation.

- Transmission des connaissances :
  - Continuer les tutoriels vidéo.
  - Transférer la documentation vers le nouveau site.

- Développement de jeux :
  - Priorité donnée à *Children of Solarus*.
  - *A Link to the Dream*
  - *Oni-Link Begins*
  - *Mercuris' Chest*

Le rapport moral a été approuvé à l'unanimité par un vote sur Discord. Sur les 6 présents, il n'y a pas eu d'abstention.

## Rapport financier

Solarus est bénéficiaire en 2021 :

- Le don de Christophe paie l'hébergement et le nom de domaine pour 2021. OVH a d'ailleurs fait un geste commercial pour réduire la facture suite aux soucis.
- La trésorie accumulée en 2021 n'a pas été dépensée.

Il est envisagé de payer un certificat afin de signer l'éxécutable et l'installeur. Sur les 6 présents, il n'y a pas eu d'abstention.

Le rapport financier a été approuvé à l'unanimité par un vote sur Discord. Sur les 6 présents, il n'y a pas eu d'abstention.

## Élection des membres du Conseil d'Administration

La décision de conserver la composition actuelle du Conseil d'Administration a été prise à l'unanimité.

Cette décision a été approuvée à l'unanimité par un vote sur Discord. Sur les 6 présents, il n'y a pas eu d'abstention.

## Fonctionnement de l'association

Aucun changement du fonctionnement de l'association n'a été réclamé ni décidé. L'association conserve le même fonctionnement qu'en 2021.

## Conclusion

Il a été dressé le présent Procès-Verbal, signé par le Président de Séance et le Secrétaire de Séance.
