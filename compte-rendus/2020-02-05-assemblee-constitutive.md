# Procès-verbal de l'Assemblée Générale Constitutive du 05/02/2020

## Introduction

Le 05/02/2020, les fondateurs de l'association *Solarus Labs* se sont réunis en Assemblée Générale Constitutive par visioconférence.

Etaient présents :

- Christophe Thiéry
- Kévin Baumann
- Olivier Cléro
- Benjamin Schweitzer

L'Assemblée Générale a désigné :

- Christophe Thiéry en qualité de Président de Séance
- Olivier Cléro en qualité de Secrétaire de Séance.

Le Président de Séance a rappellé que l'Assemblée Générale Constitutive était appelée à statuer sur l'ordre du jour suivant :

- Adoption des Statuts
- Élection des membres du Conseil d'Administration
- Nomination des membres du Bureau
- Décision du montant de la cotisation annuelle
- Attribution du rôle nécessaire aux démarches de déclaration

## Adoption des Statuts

Le Président de Séance a exposé à l'Assemblée Générale les motifs du projet de création de l'Association. Il a rendu compte des démarches déjà entreprises et des engagements pris pour le compte de l'Association. Il a lu et commenté le projet de Statuts.

Les Statuts ont été approuvés à l'unanimité.

## Élection des membres du Conseil d'Administration

Le Conseil d'Administration a été élu à l'unanimité par l'Assemblée Générale. Il est composé des membres suivants :

- Christophe Thiéry
- Kévin Baumann
- Olivier Cléro
- Benjamin Schweitzer

Les membres du Conseil d'Administration exerceront leurs fonctions conformément aux Statuts.

## Nomination des membres du Bureau

Le Conseil d'Administration a nommé, à l'unanimité, le Bureau de l'Association. Il est composé des membres suivants :

- Christophe Thiéry : Président
- Kévin Baumann : Trésorier
- Olivier Cléro : Secrétaire
- Benjamin Schweitzer : Vice-trésorier

Les membres du Bureau exerceront leurs fonctions conformément aux Statuts.

## Décision du montant de la cotisation annuelle

À l'unanimité, le Conseil d'Administration a décidé que le montant de la cotisation annuelle sera de 5€.

## Attribution du rôle nécessaire aux démarches de déclaration

L'Assemblée Générale Constitutive donne pouvoir à Christophe Thiéry d'effectuer les démarches nécessaires de constitution de l'Association : déclaration à la Préfecture, publication dans le Journal Officiel et demande d'attribution d'un numéro SIREN.

Cette décision a été prise à l'unanimité.

## Conclusion

Il a été dressé le présent Procès-Verbal, signé par le Président de Séance et le Secrétaire de Séance.
