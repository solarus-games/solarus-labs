# Procès-verbal de l'Assemblée Générale du 16/02/2025

## Introduction

Le 16/02/2025, les membres de l'association _Solarus Labs_ se sont réunis en Assemblée Générale à Montrouge, et par visioconférence pour ceux qui ne pouvaient pas être là.

Etaient présents :

1. Christophe Thiéry.
2. Kévin Baumann.
3. Olivier Cléro.
4. Thomas Noury (membre de droit).
5. Clément Boissière.

L'Assemblée Générale a désigné :

- Christophe Thiéry en qualité de Président de Séance.
- Olivier Cléro en qualité de Secrétaire de Séance.

Le Président de Séance a rappellé que l'Assemblée Générale était appelée à statuer sur l'ordre du jour suivant :

- [Rapport moral](#rapport-moral)
- [Rapport financier](#rapport-financier)
- [Élection des membres du Conseil d'Administration](#élection-des-membres-du-conseil-dadministration)
- [Fonctionnement de l'association](#fonctionnement-de-lassociation)

## Rapport moral

### Solarus Labs en 2024

#### Projets accomplis avec Solarus en 2024

- Avancement de Solarus 2.0 :

  - Une date de sortie pour 2.0 a été décidée.
  - Les ambitions ont été revues à la baisse afin de sortir 2.0 assez rapidement.
  - Le reste des fonctionnalités prévues initialement pour 2.0 mais non-terminées seront reportées pour 2.1 et versions ultérieures.
  - Grandes améliorations de l'API audio par Thomas.
  - Nombreuses améliorations de type Quality-of-Life et corrections de bugs par Christophe.
  - Mise à jour de la CI par Hugo.
  - Achat d'un Raspberry Pi pour améliorer le support de RecalBox.

- Site web de Solarus :

  - Les vidéos (trailers, tutoriels) ont été uploadées sur PeerTube, ce qui permet de ne pas avoir de trackers tierces sur le site.

- Qlementine (bibliothèque graphique Qt) :

  - Release 1.0.0
  - Utilisé en production par l'entreprise Filewave.
  - Utilisé en version de développement pour Solarus Editor, ce qui permet de bénéficier d'un thème sombre, attendu depuis longtemps par la communauté !

- Solarus Android app :

  - Réécriture de l'interface utilisateur par Steve Spear, basée sur les maquettes d'Olivier.
  - Maintenance de la compilation/CI par Hhromic.

- La communauté a sorti plusieurs jeux _Zelda_ amateurs :

  - Sortie d'une démo pour _Another Hyrule Fantasy_ par Zeldo, une refonte du premier jeu _Zelda_.
  - Sortie de _The Labors of Zeldo_ par Zeldo, une compilation d'énigmes _Zelda_.
  - Sortie d'une démo pour _Region of Deseraph_ demo release, par Rasdarac.
  - Sortie d'une démo pour le remake de _A Link to the Past_, par KaKaShUruKioRa, Zeldo, DarkDavy15 et Adenothe.

- Le développement de jeux _Zelda_ amateurs a continué :

  - _A Link to the Dream_ par l'équipe de Binbin.
  - _Mercuris' Chest_ par Metallizer.

- Des jeux non-issus de l'univers _Zelda_ ont également été publiés :

  - _Galacta Knight_, un RPG dans l'univers de Kirby, par Blazie.
  - _Long Steel_, un jeu commercial d'Action/Aventure publié sur Steam, par Kyle Tassone.

#### Projets en pause en 2024

- Portage WASM de Solarus.
- Nouveau launcher Solarus
- Migration de la documentation Doxygen vers le nouveau site MkDocs.
- Children of Solarus
- Tutoriaux textes.
- Migration vers un monorepo (qu induit une mise à jour des fichiers CMake).

### Projets envisagés en 2025

- Sortie de Solarus 2.0.0 :

  - Date choisie mais maintenue secrète.
  - Bande-annonce vidéo.
  - Mise à jour de l'ancien Solarus Launcher avec Qt6 et Qlementine.
  - Thème dark aux couleurs de Solarus pour Solarus Editor et Launcher.

- Versions de correctifs 2.0.x.

- Sont repoussés à Solarus 2.x.y ou plus tard :

  - Solarus sur RecalBox.
  - Solarus WASM.
  - Solarus Android.
  - Solarus iOS.
  - Solarus Launcher en QML.
  - Monorepo.
  - Documentation en markdown.
  - Tutoriaux textes.

- Développement de jeux :

  - _Children of Solarus_.
  - _Mercuris' Chest_.

- Évènements :

  - Session de découverte de Solarus au club des sciences du lycée René Descartes (18 février 2025).

### Approbation du rapport moral

Le rapport moral a été approuvé à l'unanimité par un vote sur Discord. Sur les 4 votants présents, il n'y a pas eu d'abstention.

## Rapport financier

Solarus est bénéficiaire en 2024 :

- Petits dons récurrents et dons ponctuels.
- La trésorerie a été utilisée pour payer les frais de serveur et les frais de déplacement pour les hackatons.
- L'association n'ayant pas encore de compte en banque, Christophe a encaissé les fonds issus du stage de création de jeu durant l'été 2024. Il a donc une dette envers l'association Solarus Labs.
- L'association possède des fonds en dollars (USD), issus d'un don dans cette monnaie.

Le rapport financier a été approuvé à l'unanimité par un vote sur Discord. Sur les 4 votants présents, il n'y a pas eu d'abstention.

## Élection des membres du Conseil d'Administration

Se sont présentés pour le Conseil d'Administration :

- Christophe Thiéry.
- Olivier Cléro.
- Kévin Baumann.

Cette composition du Conseil d'Administration a été approuvée à l'unanimité par un vote sur Discord. Sur les 4 votants présents, il n'y a pas eu d'abstention.

## Fonctionnement de l'association

Aucun changement du fonctionnement de l'association n'a été réclamé ni décidé. L'association conserve le même fonctionnement que l'année précédente.

## Nouveau(x) membre(s) de droit

Le Conseil d'Administration a décidé à l'unanimité de donner le statut de membre de droit à Clément Boissière pour son aide précieuse au développement de Solarus iOS.

## Conclusion

Il a été dressé le présent Procès-Verbal, signé par le Président de Séance et le Secrétaire de Séance.
