# Procès-verbal de l'Assemblée Générale du 18/02/2024

## Introduction

Le 18/02/2024, les membres de l'association _Solarus Labs_ se sont réunis en Assemblée Générale à Montrouge, et par visioconférence pour ceux qui ne pouvaient pas être là.

Etaient présents :

1. Christophe Thiéry.
2. Kévin Baumann.
3. Olivier Cléro.
4. Max Mraz (membre honorifique).
5. Grégoire Hirsch (membre honorifique).
6. Hugo Hromic (membre honorifique).
7. Thomas Noury (membre honorifique).

L'Assemblée Générale a désigné :

- Christophe Thiéry en qualité de Président de Séance.
- Olivier Cléro en qualité de Secrétaire de Séance.

Le Président de Séance a rappellé que l'Assemblée Générale était appelée à statuer sur l'ordre du jour suivant :

- [Rapport moral](#rapport-moral)
- [Rapport financier](#rapport-financier)
- [Élection des membres du Conseil d'Administration](#élection-des-membres-du-conseil-dadministration)
- [Fonctionnement de l'association](#fonctionnement-de-lassociation)

## Rapport moral

### Solarus Labs en 2023

#### Projets accomplis avec Solarus en 2023

- Avancement de Solarus 2.0 :

  - Multijoueur (Grégoire).
  - Nouvelles fonctionnalités audio (Christophe).
  - Stabilisation par de nombreuses corrections de bugs.
  - La version issue de branche de dévelopement a été beaucoup utilisée par Max et Thomas dans leurs jeux respectifs.
  - Progression du développement du nouveau launcher, lentement mais sûrement (Olivier).

- Le jeu _Mercuris' Chest_, annoncé il y a plus de 20 ans, renaît de ses cendres, et a connu la sortie d'une démo au printemps. Thomas travaille activement sur le développement du jeu.

- Christopho a mené un stage de développement de jeux vidéo à destination d'enfants de 11 à 13 ans, pendant une semaine en août. Il a été invité par la médiathèque d'Héricourt (Haute-Saône). Ce service a été facturé et les bénéfices sont allés dans les caisses de l'association.

- Le développement du futur thème Qt de l'éditeur Solarus a fait un grand bon, au point d'être utilisé en production par l'entreprise pour laquelle travaille Christophe. Ce projet open-source avait été débuté par Olivier, qui a travaillé en freelance pour l'entreprise, afin de développer le thème avec Christophe.

- L'application Android a progressé. Un contributeur bénévole nommé Steve Spear a entamé la refonte de l'application en Kotlin, sur la base de maquettes faites par Olivier dans Figma.

- Le site web de Solarus possède désormais une API statique qui permet d'obtenir la liste des jeux, les articles du blog, et les liens de téléchargement de Solarus.

- La communauté a continué de contribuer aux projets liés à Solarus (packs de ressources, jeux, améliorations du moteur).

- _Ocean's Heart_, développé par Max, a dépassé les 100000 exemplaires vendus (Steam et Nintendo Switch).

#### Projets en pause en 2023

- Portage WASM de Solarus.
- Développement d'_A Link to the Dream_.
- Migration de la documentation Doxygen vers le nouveau site MkDocs.

### Projets envisagés en 2024

- Sortie de Solarus 2.0 avant l'été :

  - Finir le nouveau launcher.
  - Mettre à jour la documentation (MkDocs).
  - Migration vers un monorepo (qu induit une mise à jour des fichiers CMake).

- Sont repoussés à Solarus 2.1 ou plus tard :

  - Solarurs WASM.
  - Solarus Android.

- Collaboration avec Recalbox.

- Organisation de stages Solarus.

- Développement de jeux :
  - _Children of Solarus_.
  - _Mercuris' Chest_.

Le rapport moral a été approuvé à l'unanimité par un vote sur Discord. Sur les 7 présents, il n'y a pas eu d'abstention.

## Rapport financier

Solarus est bénéficiaire en 2023 :

- Petits dons récurrents et dons ponctuels.
- La trésorerie a été utilisée pour payer les frais de serveur et les frais de déplacement pour les hackatons.

Le rapport financier a été approuvé à l'unanimité par un vote sur Discord. Sur les 7 présents, il n'y a pas eu d'abstention.

## Élection des membres du Conseil d'Administration

Se sont présentés pour le Conseil d'Administration :

- Christophe Thiéry.
- Olivier Cléro.
- Kévin Baumann.

Cette composition du Conseil d'Administration a été approuvée à l'unanimité par un vote sur Discord. Sur les 7 présents, il n'y a pas eu d'abstention.

## Fonctionnement de l'association

Aucun changement du fonctionnement de l'association n'a été réclamé ni décidé. L'association conserve le même fonctionnement que l'année précédente.

## Conclusion

Il a été dressé le présent Procès-Verbal, signé par le Président de Séance et le Secrétaire de Séance.
