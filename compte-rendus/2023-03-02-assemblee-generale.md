# Procès-verbal de l'Assemblée Générale du 02/03/2023

## Introduction

Le 02/03/2023, les membres de l'association *Solarus Labs* se sont réunis en Assemblée Générale par visioconférence.

Etaient présents :

- Christophe Thiéry.
- Kévin Baumann.
- Olivier Cléro.
- Benjamin Schweitzer.
- Max Mraz (membre honorifique).
- Grégoire Hirsch (membre honorifique).

L'Assemblée Générale a désigné :

- Christophe Thiéry en qualité de Président de Séance.
- Olivier Cléro en qualité de Secrétaire de Séance.

Le Président de Séance a rappellé que l'Assemblée Générale était appelée à statuer sur l'ordre du jour suivant :

- [Rapport moral](#rapport-moral)
- [Rapport financier](#rapport-financier)
- [Élection des membres du Conseil d'Administration](#élection-des-membres-du-conseil-dadministration)
- [Fonctionnement de l'association](#fonctionnement-de-lassociation)

## Rapport moral

### Solarus Labs en 2022

#### Projets accomplis avec Solarus en 2022

- Le jeu Ocean's Heart est sorti en janvier en version dématérialisée sur Nintendo Switch, grâce au portage réalisé par Grégoire.

- Nouvelle charte graphique par Olivier :
  - Nouveau logo.
  - Nouveau site web vitrine.
  - Nouveau site web pour la documentation.

- Du ménage a été fait dans les dépôts (moteur, outils, jeux) Git par Hugo Hromic :
  - Les dépôts obsolètes ont été archivés.
  - Des sous-groupes ont été créés pour organiser les dépôts qui commence à être nombreux.
  - Les dépôts utilisent désormais le système de *releases* afin de proposer des téléchargements aux utilisateurs.

- Les tutoriels vidéo ont été mis à jour par Christopho :
  - Il y a désormais 61 tutoriels vidéo pour Solarus 1.6.
  - Ils sont disponibles à la fois en français et en anglais.

- La communauté a continué de contribuer aux projets liés à Solarus :
  - Packs de ressources.
  - Jeux.
  - Améliorations du moteur.

#### Projets ayant progressé en 2022

- Solarus 2.0 :
  - Correction de bugs.

- Nouveau Lanceur de quêtes Solarus :
  - Correction de bugs.
  - Connexion à l'API du nouveau site.

- *A Link to the Dream* :
  - Reprise du développement.

- *Mercuris' Chest* :
  - Reprise du développement.

- Portage Android de Solarus :
  - Hugo a nettoyé le projet.
  - Des utilisateurs ont pu tester et faire des retours.

#### Projets en pause en 2022

- Thème Qt sombre pour l'Éditeur de quêtes Solarus.
- Portage WASM de Solarus.

### Projets envisagés en 2022

- Solarus 2.0 :
  - Fonctionnalités multi-joueurs.
  - Nouveau lanceur Solarus.
  - Portage Android.
  - Portage WASM.
  - Style sombre pour l'Éditeur de quête Solarus.

- Transmission des connaissances :
  - Transférer la documentation Doxygen vers le nouveau site MkDocs.

- Développement de jeux :
  - Priorité donnée à *Children of Solarus*.
  - *A Link to the Dream* : sortie possible en 2023.
  - *Oni-Link Begins*.
  - *Mercuris' Chest*.

Le rapport moral a été approuvé à l'unanimité par un vote sur Discord. Sur les 6 présents, il n'y a pas eu d'abstention.

## Rapport financier

Solarus est bénéficiaire en 2022 :

- Petits dons récurrents et dons ponctuels (dont un don conséquent de Max).
- La trésorerie a été utilisée pour payer les frais de serveur.

Le rapport financier a été approuvé à l'unanimité par un vote sur Discord. Sur les 6 présents, il n'y a pas eu d'abstention.

## Élection des membres du Conseil d'Administration

Se sont présentés pour le Conseil d'Administration :

- Christophe Thiéry.
- Olivier Cléro.
- Kévin Baumann.

Cette composition du Conseil d'Administration a été approuvée à l'unanimité par un vote sur Discord. Sur les 6 présents, il n'y a pas eu d'abstention.

## Fonctionnement de l'association

Aucun changement du fonctionnement de l'association n'a été réclamé ni décidé. L'association conserve le même fonctionnement que l'année précédente.

## Conclusion

Il a été dressé le présent Procès-Verbal, signé par le Président de Séance et le Secrétaire de Séance.
