# Procès-verbal de l'Assemblée Générale du 04/02/2021

## Introduction

Le 04/02/2021, les membres de l'Association *Solarus Labs* se sont réunis en Assemblée Générale par visioconférence.

Etaient présents :

- Christophe Thiéry
- Kévin Baumann
- Olivier Cléro
- Benjamin Schweitzer

L'Assemblée Générale a désigné :

- Christophe Thiéry en qualité de Président de Séance
- Olivier Cléro en qualité de Secrétaire de Séance.

Le Président de Séance a rappellé que l'Assemblée Générale était appelée à statuer sur l'ordre du jour suivant :

- Rapport moral
- Rapport financier
- Élection des membres du Conseil d'Administration
- Changement d'adresse
- Désignation du Bureau
- Transparence de l'Association

## Rapport moral

### Solarus Labs en 2020

#### Projets accomplis avec Solarus en 2020

- Solarus 1.6.3 et Solarus 1.6.4 :
  - Port macOS.
  - Meilleure stabilité.
  - Tilesets libres (Ocean Set).

- Évènements de la communauté :
  - Vente de goodies.
  - Hommage vidéo à Bob Ross.

- Nombreuses contributions venant de la communauté :
  - Visual Novel System : système de dialogues avancés.
  - Plusieurs packs de ressources.
  - Divers scripts.
  - Avancement sur Children of Solarus.

- Solarus a fait parler de lui :
  - Intégration dans Recalbox 7.0.
  - Liège Game Lab : une université utilise Solarus à des fins éducatives.
  - Yarntown : court jeu hommage à Bloodborne qui a beaucoup buzzé.
  - Ocean's Heart : premier jeu commercial fait avec Solarus (disponible sur Steam).

Pour ces raisons, on peut dire que Solarus arrive à maturité.

#### Projets ayant peu progressé en 2020

- Tutoriels (écrits et vidéo) qui ont peu avancé.
- Projet de collaboration avec un institut qui n'a pas (encore ?) abouti.
- Développement de jeux au point mort :
  - Zelda Oni-Link Begins.
  - Children of Solarus.
- Sortie du nouveau lanceur repoussée.

### Projets envisagés en 2021

- Solarus 1.6.5
  - Correction de bugs.
  - Améliorations pour Ocean's Heart.
  - Correction du bug légendaire de la touche R lors du redimensionnement des tiles.
  - Projet top-secret.

- Solarus ~~1.7~~ **2.0** :
  - Fonctionnalités multi-joueurs (pratiquement fini).
  - Nouveau lanceur Solarus.
  - Portage Android.
  - Style sombre pour l'Éditeur de Quête Solarus.
  - Nouveau logo.
  - Rafraîchissement du site Web.

- Transmission des connaissances :
  - Relancer les tutoriels.

- Développement de jeux :
  - Priorité donnée à Children of Solarus.

- Goodies :
  - Masques hygiéniques Solarus Labs ?
  - Nouveaux stickers avec Stickermule.

Le rapport moral a été approuvé à l'unanimité.

## Rapport financier

Solarus est bénéficiaire en 2020 :

- Le don de Christophe offre l'hébergement et le nom de domaine pour 2020.
- Un premier bounty concernant des améliorations de l'API son représente une grosse part de la trésorerie.
- Cela permet à Christophe de ne pas avoir à payer les frais du serveur pour 2021.

Questions autour de l'organisation :

- Il a été décidé que le fichier du tableur des finances de l'Association serait mis sur Google Drive.

- Le Trésorier n'a pas de façon simple d'accéder aux compte PayPal de Solarus Labs, en raison de l'authentification à deux facteurs associée au numéro de téléphone du Président. Il a été discuté de changer le numéro associté au compte Paypal pour y mettre celui du trésorier actuel, Kévin.

- L'association n'a toujours pas de compte bancaire officiel car elle n'en n'a pas encore l'utilité.

Le rapport financier a été approuvé à l'unanimité.

## Élection des membres du Conseil d'Administration

La décision de conserver la composition actuelle du Conseil d'Administration a été prise à l'unanimité.

## Membres de droit

Certaines personnes contribuent activement au projet Solarus, et ne sont pas encore membres de l'Association. Il a été discuté de nommer membres de droit, avec leurs accords, les personnes suivantes, en raison de leurs contributions exceptionnelles à la communauté :

- Grégoire Hirt
- Max Mraz
- Hugo Hromic

Cette décision a été prise l'unanimité. Ces personnes deviendront officiellement membres de droit de l'Association une fois qu'elles auront donné leur accord.

## Cotisation

Jusqu'à présent, le montant de la cotisation d'adhésion à l'Association n'avait pas été discutée. Après réflexion, le montant de la cotisation d'adhésion a été fixé à **5,00€**.

Cette décision a été prise à l'unanimité.

Les membres de l'Association recevront un document au format PDF attestant de leur adhésion à l'Association.

## Changement d'adresse

Christophe a déménagé en 2020. Le siège social de l'Association est jusqu'aujourd'hui à son ancienne adresse. Il propose de changer l'adresse du siège social de l'Association pour qu'il corresponde à sa nouvelle adresse.

Cette décision a été prise à l'unanimité.

## Désignation du Bureau

Le Conseil d'Administration a nommé, à l'unanimité, le Bureau de l'Association. Il est composé des membres suivants :

- Christophe Thiéry : Président
- Kévin Baumann : Trésorier
- Olivier Cléro : Secrétaire
- Benjamin Schweitzer : Vice-trésorier

## Transparence de l'Association

A des fins de transparence la plus totale, le dépôt Git des comptes-rendus des procès verbaux des différentes réunions de l'Association sera ouvert à toutes et à tous, en devenant public. Les *forks* sont cependant bloqués. Seuls les membres du Bureau ont les droits de modifier ce dépôt.

Cette décision a été prise à l'unanimité.

## Conclusion

Il a été dressé le présent Procès-Verbal, signé par le Président de Séance et le Secrétaire de Séance.
